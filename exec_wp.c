#include <unistd.h>
#include <stdio.h>

pid_t exec_wp
(
	char** args,
	int stdin_pipe[2],
	int stdout_pipe[2]
)
{
	pid_t child;
	if(child = fork())
	{
		return child;
	}
	else
	{
		if(stdin_pipe) dup2(stdin_pipe[0], 0), close(stdin_pipe[1]);
		if(stdout_pipe) dup2(stdout_pipe[1], 1), close(stdout_pipe[0]);
		if(execvp(args[0], args) == -1) {
			perror("execvp");
		}
	}
}



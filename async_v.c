#include <stdio.h>

#include "async.h"

pid_t async_v(char** args)
{
	printf("$");
	for(char** m = args;*m;m++)
	{
		printf(" \"%s\"", *m);
	}
	printf("\n");
	return async(args);
}


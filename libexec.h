#ifndef libexec_H
#define libexec_H

int exec(char** args);

int exec_v(char** args);
#include <unistd.h>

pid_t exec_wfd
(
	char** args,
	int stdin_fd,
	int stdout_fd
);
#include <sys/types.h>

pid_t async(char** args);
pid_t async_v(char** args);
pid_t exec_wp
(
	char** args,
	int stdin_pipe[2],
	int stdout_pipe[2]
);
#endif

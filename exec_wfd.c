#include <unistd.h>
#include <stdio.h>

pid_t exec_wfd
(
	char** args,
	int stdin_fd,
	int stdout_fd
)
{
	pid_t child;
	if(child = fork())
	{
		return child;
	}
	else
	{
		if(stdin_fd) dup2(stdin_fd, 0);
		if(stdout_fd) dup2(stdout_fd, 1);
		if(execvp(args[0], args) == -1) {
			perror("execvp");
		}
	}
}



#include <stdio.h>

#include "exec.h"

int exec_v(char** args)
{
	printf("$");
	for(char** m = args;*m;m++)
	{
		printf(" \"%s\"", *m);
	}
	printf("\n");
	return exec(args);
}


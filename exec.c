#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

// returns status (man waitpid)

int exec(char** args)
{
	pid_t child;
	int status = 0;
	if(child = fork())
	{
		if(waitpid(child, &status, 0) == -1) {
			perror("waitpid (execlib)");
		}
	}
	else
	{
		if(execvp(args[0], args) == -1) {
			perror("execvp");
		}
	}
	return status;
}


